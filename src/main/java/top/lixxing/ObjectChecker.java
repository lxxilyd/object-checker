package top.lixxing;

import top.lixxing.exception.CheckException;
import top.lixxing.function.CallBack;
import top.lixxing.function.Getter;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * 对象校验器
 * @Author lxx
 * @create 2023年6月28日 028 10:15:46
 */
public class ObjectChecker<T> {

	private final T target;
	private final Map<Predicate, Object> testMap = new LinkedHashMap<>();
	private final Map<Predicate, Object> targetMap = new LinkedHashMap<>();

	public static final Predicate<? super Object> IS_NULL = ObjectChecker::isNull;
	public static final Predicate<? super Object> NOT_NULL = ObjectChecker::notNull;
	public static final Predicate<? super Collection<?>> IS_EMPTY = ObjectChecker::isEmpty;
	public static final Predicate<? super Collection<?>> NOT_EMPTY = ObjectChecker::isNotEmpty;
	public static final Predicate<? super CharSequence> IS_BLANK = ObjectChecker::isBlank;
	public static final Predicate<? super CharSequence> NOT_BLANK = ObjectChecker::isNotBlank;

	private CallBack success;
	private CallBack fail;

	private ObjectChecker(T target) {
		this.target = target;
	}

	/**
	 * @param target 校验的目标对象
	 * @param <T> 目标对象类型
	 * @return 校验器
	 */
	public static <T> ObjectChecker<T> target(T target) {
		return new ObjectChecker<>(target);
	}

	/**
	 * @param test 校验逻辑
	 * @param message 校验逻辑为true返回的校验信息
	 * @return 校验器
	 */
	public ObjectChecker<T> valid(Predicate<? super T> test, String message) {
		testMap.put(test, message);
		targetMap.put(test, target);
		return this;
	}

	/**
	 * @param test 校验逻辑
	 * @param callBack 校验逻辑为true时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public ObjectChecker<T> valid(Predicate<? super T> test, CallBack callBack) {
		testMap.put(test, callBack);
		targetMap.put(test, target);
		return this;
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param test 校验逻辑
	 * @param message 校验逻辑为true返回的校验信息
	 * @return 校验器
	 */
	public <V> ObjectChecker<T> valid(Getter<T, V> getter, Predicate<? super V> test, String message) {
		V field = getter.apply(this.target);
		testMap.put(test, message);
		targetMap.put(test, field);
		return this;
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param test 校验逻辑
	 * @param callBack 校验逻辑为true时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public <V> ObjectChecker<T> valid(Getter<T, V> getter, Predicate<? super V> test, CallBack callBack) {
		V field = getter.apply(this.target);
		testMap.put(test, callBack);
		targetMap.put(test, field);
		return this;
	}

	/**
	 * @param message 对象为空时返回的校验信息
	 * @return 校验器
	 */
	public ObjectChecker<T> isNull(String message) {
		return isNull(t -> target, message);
	}

	/**
	 * @param callBack 对象为空时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public ObjectChecker<T> isNull(CallBack callBack) {
		return isNull(t -> target, callBack);
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param message 对象为空时返回的校验信息
	 * @return 校验器
	 */
	public <V> ObjectChecker<T> isNull(Getter<T, V> getter, String message) {
		testMap.put(IS_NULL, message);
		targetMap.put(IS_NULL, getter.apply(target));
		return this;
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param callBack 对象为空时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public <V> ObjectChecker<T> isNull(Getter<T, V> getter, CallBack callBack) {
		testMap.put(IS_NULL, callBack);
		targetMap.put(IS_NULL, getter.apply(target));
		return this;
	}

	/**
	 * @param message 对象为集合且集合空时返回的校验信息
	 * @return 校验器
	 */
	public ObjectChecker<T> isEmpty(String message) {
		if (target instanceof Collection) {
			return isEmpty(t -> (Collection<?>) target, message);
		}
		throw new CheckException(target.getClass().getName() + " is not a " + Collection.class.getName());
	}

	/**
	 * @param callBack 对象为集合且集合空时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public ObjectChecker<T> isEmpty(CallBack callBack) {
		if (target instanceof Collection) {
			return isEmpty(t -> (Collection<?>) target, callBack);
		}
		throw new CheckException(target.getClass().getName() + " is not a " + Collection.class.getName());
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param message 对象为集合且集合空时的校验信息
	 * @return 校验器
	 */
	public <V extends Collection<?>> ObjectChecker<T> isEmpty(Getter<T, V> getter, String message) {
		testMap.put(IS_EMPTY, message);
		targetMap.put(IS_EMPTY, getter.apply(target));
		return this;
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param callBack 对象为集合且集合空时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public <V extends Collection<?>> ObjectChecker<T> isEmpty(Getter<T, V> getter, CallBack callBack) {
		testMap.put(IS_EMPTY, callBack);
		targetMap.put(IS_EMPTY, getter.apply(target));
		return this;
	}

	/**
	 * @param message 对象为字符串且字符串为空时的校验信息
	 * @return 校验器
	 */
	public ObjectChecker<T> isBlank(String message) {
		if (target instanceof CharSequence) {
			return isBlank(t -> (CharSequence) target, message);
		}
		throw new CheckException(target.getClass().getName() + " is not a " + CharSequence.class.getName());
	}

	/**
	 * @param callBack 对象为字符串且字符串为时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public ObjectChecker<T> isBlank(CallBack callBack) {
		if (target instanceof CharSequence) {
			return isBlank(t -> (CharSequence) target, callBack);
		}
		throw new CheckException(target.getClass().getName() + " is not a " + CharSequence.class.getName());
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param message 对象为字符串且字符串为空时的校验信息
	 * @return 校验器
	 */
	public <V extends CharSequence> ObjectChecker<T> isBlank(Getter<T, V> getter, String message) {
		testMap.put(IS_BLANK, message);
		targetMap.put(IS_BLANK, getter.apply(target));
		return this;
	}

	/**
	 * @param getter 目标对象的字段Getter方法
	 * @param callBack 对象为字符串且字符串为时的回调方法（返回校验信息）
	 * @return 校验器
	 */
	public <V extends CharSequence> ObjectChecker<T> isBlank(Getter<T, V> getter, CallBack callBack) {
		testMap.put(IS_BLANK, callBack);
		targetMap.put(IS_BLANK, getter.apply(target));
		return this;
	}

	/**
	 * @param success 校验通过的回调方法
	 * @return
	 */
	public ObjectChecker<T> onSuccess(CallBack success) {
		this.success = success;
        return this;
	}

	/**
	 * @param fail 校验失败的回调方法
	 * @return
	 */
	public ObjectChecker<T> onFail(CallBack fail) {
		this.fail = fail;
		return this;
	}

	/**
	 * 做校验并返回校验信息
	 * @return 校验返回信息
	 */
	public String check() {
		for (Map.Entry<Predicate, Object> entry : testMap.entrySet()) {
			if (entry.getKey().test(targetMap.get(entry.getKey()))) {
				if (fail != null) {
					fail.apply();
				}
				Object value = entry.getValue();
				if (value instanceof CallBack) { {
					return ((CallBack) value).apply();
				}}
				return value.toString();
			}
		}
		if (success != null) {
			success.apply();
		}
		return null;
	}

	/**
	 * 做校验并抛出异常
	 */
	public void checkAndThrow() {
		for (Map.Entry<Predicate, Object> entry : testMap.entrySet()) {
			if (entry.getKey().test(targetMap.get(entry.getKey()))) {
				if (fail != null) {
					fail.apply();
				}
				Object value = entry.getValue();
				if (value instanceof CallBack) { {
					String message = ((CallBack) value).apply();
					throw new CheckException(message);
				}}
				throw new CheckException(value.toString());
			}
		}
		if (success != null) {
			success.apply();
		}
	}

	/**
	 * 做校验并抛出异常
	 * @param throwableFunction 异常类构造方法
	 */
	public void checkAndThrow(Function<String, ? extends RuntimeException> throwableFunction)  {
		for (Map.Entry<Predicate, Object> entry : testMap.entrySet()) {
			if (entry.getKey().test(targetMap.get(entry.getKey()))) {
				if (fail != null) {
					fail.apply();
				}
				Object value = entry.getValue();
				if (value instanceof CallBack) { {
					String message = ((CallBack) value).apply();
					throw throwableFunction.apply(message);
				}}
				throw throwableFunction.apply(value.toString());
			}
		}
		if (success != null) {
			success.apply();
		}
	}

	/**
	 * 做校验并抛出异常
	 * @param throwableClass 异常类
	 */
	public void checkAndThrow(Class<? extends RuntimeException> throwableClass) {
		for (Map.Entry<Predicate, Object> entry : testMap.entrySet()) {
			if (entry.getKey().test(targetMap.get(entry.getKey()))) {
				try {
					if (fail != null) {
						fail.apply();
					}
					Object value = entry.getValue();
					if (value instanceof CallBack) { {
						String message = ((CallBack) value).apply();
						throw throwableClass.getDeclaredConstructor(String.class).newInstance(message);
					}}
					throw throwableClass.getDeclaredConstructor(String.class).newInstance(value);
				} catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
					throw new CheckException(e);
				}
			}
		}
		if (success != null) {
			success.apply();
		}
	}

	public static boolean notNull(Object obj) {
		return !isNull(obj);
	}

	public static boolean isNull(Object object) {
		return object == null;
	}

	public static boolean isNotEmpty(Collection<?> collection) {
		return collection != null && !collection.isEmpty();
	}

	public static boolean isEmpty(Collection<?> collection) {
		return !isNotEmpty(collection);
	}

	public static boolean isBlank(final CharSequence cs) {
		int strLen;
		if (cs == null || (strLen = cs.length()) == 0) {
			return true;
		}
		for (int i = 0; i < strLen; i++) {
			if (!Character.isWhitespace(cs.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	public static boolean isNotBlank(final CharSequence cs) {
		return !isBlank(cs);
	}
}
