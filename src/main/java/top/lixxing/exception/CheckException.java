package top.lixxing.exception;

/**
 * @Author lxx
 * @create 2023年6月28日 028 11:59:13
 */
public class CheckException extends RuntimeException {

	public CheckException(){
		super();
	}

	public CheckException(String message){
		super(message);
	}

	public CheckException(Throwable throwable){
		super(throwable);
	}

	public CheckException(String message, Throwable throwable){
		super(message, throwable);
	}
}
