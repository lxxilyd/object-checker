package top.lixxing.function;

/**
 * @param <T> 对象类型
 * @param <V> 对象字段类型
 */
@FunctionalInterface
public interface Getter<T, V> {

    /**
     * Getter方法
     * @param t 目标对象
     * @return 目标对象的属性值
     */
    V apply(T t);
}