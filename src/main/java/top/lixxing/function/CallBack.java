package top.lixxing.function;

/**
 * 回调方法函数接口
 */
@FunctionalInterface
public interface CallBack {
    /**
     * 返回校验信息
     * @return message
     */
    String apply();
}