package top.lixxing;

import org.junit.Test;

public class ObjectCheckerTest {

    @Test
    public void check() {

        Student student = new Student();

        ObjectChecker.target(student)
                .isNull(s -> s, () -> {
                    String message = "id不能为空";
                    System.out.println(message);
                    return message;
                })
                .valid(s -> s.getName() == null, "name is null")
                .checkAndThrow(IllegalArgumentException::new);
    }
}